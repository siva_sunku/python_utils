"""
This is used to move the duplicate photos/files to archive folder.
How it works:
main_folder : Source folder where all the photos are placed
archive_folder: Folder where the photos are placed which are marked as duplicate by the program
One has to delete archive folder manually, considering the risk involved in automated deletion
PS: Negative conditions are rarely tested, please exercise caution
todo: add the report fields meaning
todo: add logger
"""

import pandas as pd
import glob
import os
import hashlib
import shutil


def get_hash(fname, buf_size=4096):
    hash_md5 = hashlib.md5()
    if os.path.isdir(fname):
        return 'HASH_DIRECTORY'
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(buf_size), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def archive_record(in_rec):
    """
    :param in_rec:
        in_rec is record from the report created.
        All the files which are duplicates move to archive
    :return:
    """
    source_fpath = in_rec.full_path
    dest_fpath = in_rec.archive_path

    if in_rec.is_duplicate == 'FALSE' or in_rec.is_duplicate == False:
        return 'Unique_File'

    if not os.path.isfile(source_fpath):
        return 'File_Not_Present'

    dest_dir = os.path.dirname(dest_fpath)
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    shutil.copy(source_fpath, dest_fpath)
    return 'File_Moved'


def duplicate_process_main(
        source_folder,
        archive_folder,
        duplicate_pattern='WITH_HASH_ONLY',
        output_report_name='duplicate_files_report.csv',
        file_search_pattern='*',
        execution_type='REPORT_AND_ARCHIVE',
        archive_from_report=None):
    """
    Main function/driver for reporting and finding duplicates
    :param source_folder: Main folder in which duplicate files to be found
    :param archive_folder: where to move the duplicate files
    :param duplicate_pattern:
     WITH_HASH_ONLY: Find hash tag of each and every file, compare hash tags and mark duplicates
     WITH_NAME_SIZE_HASH: Find duplicate file name (folder may be different)
              If file name & size are same, for such files find hash tag.
              If hash tag is same, mark as duplicate
              In this option, hash won't be calculated for all, but only for files where name & size are same
              This option can be used, if you know that while dumping folders file_name is not changed
     WITH_HASH_ONLY is a performance overhead, but finds all duplicates.
    :param output_report_name:
     Name of the report to be created.
    :param file_search_pattern:
     If you want to search only specific pattern. *.jpeg etc.
     Default: *
    :param execution_type:
     REPORT_ONLY: Only report the duplicates to be archived, do not perform actual archival
     REPORT_AND_ARCHIVE: Report the duplicates & archive the duplicates
     ARCHIVE_FROM_REPORT: Supply a report via . This report should be output from this program.
                Other reports, may give wrong results
    :param archive_from_report:
     This is the report which will be considered for ARCHIVE_FROM_REPORT
    :return: new report dataframe on actions performed against each file/row
    """
    valid_exec_types = ['ARCHIVE_FROM_REPORT', 'REPORT_ONLY', 'REPORT_AND_ARCHIVE']
    if execution_type not in valid_exec_types:
        raise Exception("Bad execution_type supplied. valid_exec_types", valid_exec_types)

    valid_dup_patterns = ['WITH_HASH_ONLY', 'WITH_NAME_SIZE_HASH']
    if duplicate_pattern not in valid_dup_patterns:
        raise Exception("Bad duplicate_pattern supplied. valid duplicate_patterns", valid_dup_patterns)

    # if execution type is ARCHIVE_FROM_REPORT, archive from the provided report
    # todo: check the sanity of the report
    if execution_type == 'ARCHIVE_FROM_REPORT':
        df_rep = pd.read_csv(archive_from_report)
        df_rep['Archive_status'] = df_rep.apply(archive_record, axis=1)
        df_rep.to_csv(output_report_name, index=False)
        return df_rep

    # find the files and drop directories if returned from glob.glob
    source_files = glob.glob(source_folder + '/**/' + file_search_pattern, recursive=True)
    df_inp = pd.DataFrame({'full_path': source_files})
    df_inp['is_folder'] = df_inp.full_path.apply(os.path.isdir)
    df_inp = df_inp[df_inp.is_folder == False]
    df_inp.drop(['is_folder'], axis='columns', inplace=True)

    # Get the stats of each file in df_dtls
    df_dtls = df_inp.copy()
    df_dtls['file_size'] = df_dtls.full_path.apply(os.path.getsize)
    df_dtls['full_folder_path'] = df_dtls.full_path.apply(os.path.dirname)
    df_dtls['only_file_name'] = df_dtls.full_path.apply(os.path.basename)
    # path relative to source_folder
    df_dtls['rel_source_path'] = df_dtls.apply(
        lambda x: os.path.relpath(x['full_path'], source_folder), axis=1)
    # path where file will be moved if it needs to be archived
    df_dtls['archive_path'] = df_dtls.apply(
        lambda x: os.path.join(archive_folder, x.rel_source_path), axis=1)
    df_dtls['hash_value'] = 'NOT_CALCULATED'

    # Calculate hash for all the files
    if duplicate_pattern == 'WITH_HASH_ONLY':
        df_dtls['hash_value'] = df_dtls.full_path.apply(get_hash)
        dup_cols = ['hash_value']

    # Find hash value only for the files with repeated file_name & size
    if duplicate_pattern == 'WITH_NAME_SIZE_HASH':
        dup_cols = ['only_file_name', 'file_size']
        df_dtls['name_size_dup'] = df_dtls.duplicated(dup_cols, keep='false')
        df_dtls['hash_value'] = df_dtls.apply(
            lambda x: get_hash(x.full_path) if x.name_size_dup else 'NOT_CALCULATED')
        dup_cols = dup_cols + ['hash_value']
        df_dtls.drop(['name_size_dup'], axis='columns', inplace=True)

    # Find the duplicate files according to the duplicate_pattern
    # has_duplicate:marks all duplicate files. if fileA, fileB are duplicates, both are marked as True
    # is_duplicate:marks real duplicate files. if fileA,fileB,fileC are dups. fileB,fileC are marked as True
    df_dtls['has_duplicate'] = df_dtls.duplicated(dup_cols, keep=False)
    df_dtls['is_duplicate'] = df_dtls.duplicated(dup_cols, keep='first')
    df_right = df_dtls[df_dtls.is_duplicate]
    df_right.rename({'full_path': 'duplicate_file_path'}, inplace=True, axis='columns')
    tmp_cols = dup_cols + ['duplicate_file_path']
    df_right = df_right[tmp_cols]
    df_rep = pd.merge(
        df_dtls,
        df_right,
        how='left',
        left_on=dup_cols,
        right_on=dup_cols
    )

    # Create a report
    df_rep.to_csv(output_report_name, index=False)
    if execution_type == 'REPORT_ONLY':
        df_rep['Archive_status'] = 'To_Be_Archived'
        return df_rep

    # Actual archive
    if execution_type == 'REPORT_AND_ARCHIVE':
        df_rep.to_csv(output_report_name, index=False)
        df_rep['Archive_status'] = df_rep.apply(archive_record, axis=1)
        return df_rep


if __name__ == '__main__':
    duplicate_pattern = 'WITH_HASH_ONLY'
    output_report_name = 'report_output.csv'
    file_name_pattern = '*'  # If restrict to jpeg '*.jpeg'
    execution_type = 'ARCHIVE_FROM_REPORT'
    source_folder = 'C:\\Users\\sivas\\Documents\\Books\\Telugu Novels'
    archive_folder = 'C:\\Users\\sivas\\Documents\\Books\\Archive'
    duplicate_process_main(source_folder=source_folder,
                           archive_folder=archive_folder,
                           execution_type=execution_type,
                           archive_from_report='duplicate_files_report.csv'
                           )
