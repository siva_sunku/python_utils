from bs4 import BeautifulSoup
import requests
import os
import numpy as np
import pdfkit

# Contstants
begin_html = '<html xmlns="http://www.w3.org/1999/xhtml"><head><body>'
end_html = '</body></head>'
data_folder = 'C:\\Users\\sivas\\Documents\\Books\\Telugu Novels'

# Functionality
os.chdir(data_folder)
def makeNovel(novel_url, novel_name, progress_Flag=False):
    """
    :param novel_url: First page url of a particular novel
    :param novel_name: File name of the novel to be saved
    :param progress_Flag: Show the progress flag
    :return: True
    """
    page = requests.get(novel_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    # All pages
    if progress_Flag:
        print("Started making novel", novel_name)
    tmp = soup.find('section',class_="bookListSort")
    tmp_urls = tmp.find_all('option')
    all_page_urls = [x['value'] for x in tmp_urls]
    all_page_urls = [x for x in all_page_urls if 'http' in x]

    # Get Text of each url from all_page_urls
    all_text=[]
    all_text.append(begin_html)

    for each_page in all_page_urls:
        if progress_Flag:
            print("Downloading the page:%s , No: %s", each_page, len(all_text))
        page = requests.get(each_page)
        soup = BeautifulSoup(page.content, 'html.parser')
        text = soup.find_all('section', id="booktext")
        all_text.append(text[0])
    all_text.append(begin_html)
    novel_name = novel_name + '.html' if 'html' not in novel_name else novel_name
    with open(novel_name, 'w', encoding='utf-8') as file:
        for line in all_text:
            file.write(str(line))

    return True

print('starting Novel Downloading')
# Ammayee O Ammayee
# Ammayee = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D-1-1588-52487.html'
# makeNovel(Ammayee, 'Ammayee.html')

# # Vesavi Vennela
# vesavi = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-248-43764.html'
# makeNovel(vesavi, 'Vesavi Vennela.html')
#
# # Bommarillu
# bommarillu = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-236-43621.html'
# makeNovel(bommarillu, 'Bommarillu.html')
#
# # Manchivadu
# manchivadu = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-1467-46142.html'
# makeNovel(manchivadu, 'Manchivadu.html')

# # Bandhithudu
# bandhithudu = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-1079-26211.html'
# makeNovel(bandhithudu,'Bandhithudu.html')

# # avamIndrajith
# avamIndrajith = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D---1-1035-24633.html'
# makeNovel(avamIndrajith,'avamIndrajith.html')

# # Maro Manasu Katha
# url='https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-1032-24559.html'
# makeNovel(url, 'Maro Manasu Katha.html')

# Manini Manasu
# url='https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D--1-980-22959.html'
# makeNovel(url, 'Manini Manasu.html')

# Hrudhayanjali
# url = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D-1-870-15969.html'
# makeNovel(url, 'Hrudhayanjali.html', progress_Flag=True)

# Sandhya kalyanam
# url = 'https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D-1-871-15813.html'
# makeNovel(url, 'Sandhya Kalyanam.html', progress_Flag=True)
#
# # Visishta
# url='https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D-1-873-16246.html'
# makeNovel(url, 'Visishta.html', progress_Flag=True)
#
# # Pelli Mantalu
# url='https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D---1--798-12402.html'
# makeNovel(url,'Pelli Mantalu.html', progress_Flag=True)
#
# Akhari Veedkolu
url='https://www.teluguone.com/grandalayam/novels/%E0%B0%8E%E0%B0%AA%E0%B0%BF%E0%B0%B8%E0%B1%8B%E0%B0%A1%E0%B1%8D---1-866-15563.html'
makeNovel(url,'Akhari Veedklu.html', progress_Flag=True)

